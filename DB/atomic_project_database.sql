-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 03, 2017 at 12:37 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `user_id` int(11) NOT NULL,
  `birthday` date NOT NULL,
  `user_name` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`user_id`, `birthday`, `user_name`, `soft_delete`) VALUES
(1, '0000-00-00', 'Ovi', 'No'),
(6, '1994-01-30', 'Ovi', 'Yes'),
(10, '1994-01-30', 'Ovi', 'Yes'),
(12, '1991-11-11', 'Anik', 'No'),
(13, '1999-12-12', 'Kalam', 'Yes'),
(14, '1999-12-24', 'Kalam', 'No'),
(15, '2001-01-01', 'Misu', 'No'),
(16, '1992-02-14', 'Gopal', 'No'),
(19, '2017-02-01', 'Ovi', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(1111) NOT NULL,
  `book_id` int(11) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'No',
  `user_name` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`book_name`, `author_name`, `book_id`, `soft_delete`, `user_name`) VALUES
('Ami Topu', 'Zafar Iqbal', 1, 'No', ''),
('Pother Panchali', 'Bivutivushan bondhopadhay', 2, 'No', ''),
('Aronnok', 'Bivutivushan bondhopadhay', 3, 'No', ''),
('Dipu No. 2', 'Zafar iqbal', 4, 'No', ''),
('Apur Shongshar.', 'Bivutivushan bondhopadhay', 5, 'No', '');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `user_id` int(11) NOT NULL,
  `city` varchar(11) NOT NULL,
  `post_code` int(111) NOT NULL,
  `post_office` varchar(111) NOT NULL,
  `police_station` varchar(111) NOT NULL,
  `detail_address` varchar(111) NOT NULL,
  `user_name` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`user_id`, `city`, `post_code`, `post_office`, `police_station`, `detail_address`, `user_name`, `soft_delete`) VALUES
(1, 'Chitttagong', 4000, 'Dampara', 'Khulshi', 'chowdhury medico', 'Ovi', 'No'),
(2, 'Chitttagong', 4000, 'Chaktai', 'Bakalia', 'Mir ahmmed Soudagar building ', 'Misu', 'No'),
(3, 'Chitttagong', 2555, 'A k khan', 'alonkar', 'monsurabaad', 'Kalam', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `user_id` int(11) NOT NULL,
  `email` varchar(111) NOT NULL,
  `user_name` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`user_id`, `email`, `user_name`, `soft_delete`) VALUES
(1, 'ovichowdhury393@gmail.com', 'Ovi', 'No'),
(2, 'kalam@gmail.com', 'Kalam', 'No'),
(4, 'anikdey@gmail.com', 'Misu', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `user_id` int(11) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `user_name` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`user_id`, `gender`, `user_name`, `soft_delete`) VALUES
(1, 'Male', 'Ovi', 'No'),
(2, 'male', 'Anik', 'No'),
(3, 'male', 'Kalam', 'No'),
(4, 'male', 'Fahad', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(111) NOT NULL,
  `hobbies` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`user_id`, `user_name`, `hobbies`, `soft_delete`) VALUES
(1, 'Ovi', 'cricket', 'No'),
(3, 'Misu', 'gardening,,photography,,programming,', 'No'),
(6, 'Kalam', 'gardening,gammeing,photography,,,', 'Yes'),
(7, 'Imran', 'gardening,gammeing,photography,,,', 'No'),
(8, 'Tanjil', 'gardening,,gammeing,programming', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(111) NOT NULL,
  `profile_picture` varchar(111) NOT NULL,
  `soft_delete` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`user_id`, `user_name`, `profile_picture`, `soft_delete`) VALUES
(7, 'Gopal', '14858009957A Lord KRISHANA.jpg', 'No'),
(8, 'Krishna', '148585555111208665_868911219812301_2269381650115960959_n.jpg', 'No'),
(9, 'Jagannath', '14858558086182771.jpg', 'No'),
(10, 'Krishna', '1485958007GOPAL_11(2).jpg', 'No'),
(12, 'Misu', '1485964692IMG_20150629_114547.jpg', 'Yes'),
(13, 'Anik', '1486116089IMG_20150629_114911.jpg', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(111) NOT NULL,
  `summary_of_organization` varchar(111) NOT NULL,
  `organization_name` varchar(111) NOT NULL,
  `soft_delete` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`user_id`, `user_name`, `summary_of_organization`, `organization_name`, `soft_delete`) VALUES
(3, 'Ovi', 'Great Organization', 'BASIS', 'No'),
(4, 'Kalam', 'This is great', 'Premier University', 'No'),
(5, 'Misu', 'I think this is good!', 'Premier University', 'Yes'),
(6, 'Anik', 'I think this is good!', 'Premier University', 'No'),
(7, 'Ovi', 'it is a good organization', 'Premier University', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`book_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `book_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
