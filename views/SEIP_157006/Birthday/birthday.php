<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
    if(!isset($_SESSION))
    {
        session_start();
    }

    $msg = Message::getMessage();
    echo "<div id= 'message'> $msg </div>";



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Birthday</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>
    form{
        width: 600px;
        margin: 0 auto;
    }
        .bg-color{
            background-color: #e2e2e2;
            color: #28a4c9;
        }
        input{
            color: #000000;
        }

    </style>
</head>
<body>
<div class="container bg-color">
<form class="form-group" action="storeBirthday.php" method="post">
    <h4>Enter The Name:</h4>
    <input type="text" name="user_name">
    <h4>Enter Birthday:</h4>
    <input type="date" name="birthday">
    <input type="submit" class="btn btn-info">
</div>
</body>
</html>