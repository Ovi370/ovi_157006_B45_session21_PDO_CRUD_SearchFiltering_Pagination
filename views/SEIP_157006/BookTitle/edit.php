<?php
require_once("../../../vendor/autoload.php");
$objBookTitle = new \App\BookTitle\BookTitle();
$objBookTitle->setData($_GET);
$oneData = $objBookTitle->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Book Title</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="../../../resource/style.css">
</head>
<body>


<form action="update.php" method="post">

    Edit Book name:
    <input type="text" name="bookName" value="<?php echo $oneData->book_name ?>">
    <br>
    Edit Author Name:
    <input type="text" name="authorName" value="<?php echo $oneData->author_name ?>">
    <br>
    <input type="hidden" name="id" value="<?php echo $oneData->book_id ?>">
    <input type="submit">



</form>

</body>
</html>