<?php
require_once("../../../vendor/autoload.php");
$objCity = new \App\City\City();
$objCity->setData($_GET);
$oneData = $objCity->view();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit City</title>

    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/style.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #5bc0de;
        }
        table{
            border: 1px;

        }
        td{
            border: 0px;
        }
    </style>
</head>
<body>

<div class="container bg-color">
    <form action="update.php" method="post">
        <input type="text" name="user_name" value="<?php echo $oneData->user_name ?>">
        <h4>Edit City:</h4>
        <input type="text" name="city" value="<?php echo $oneData->city ?>">
        <h4>Edit Post Code</h4>
        <input type="number" name="post_code" value="<?php echo $oneData->post_code ?>">
        <h4>Edit Post Office</h4>
        <input type="text" name="post_office" value="<?php echo $oneData->post_office  ?>">
        <h4>Edit Police Station</h4>
        <input type="text" name="police_station" value="<?php echo $oneData->police_station ?>">
        <h4>Edit Detail Address</h4>
        <input type="text" name="detail_address" value="<?php echo $oneData->detail_address?>">
        <input type="hidden" name="id" value="<?php echo $oneData->user_id ?>">
        <input type="submit">



    </form>
</div>
</body>
</html>