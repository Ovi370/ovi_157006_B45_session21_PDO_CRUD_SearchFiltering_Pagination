<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))
{
    session_start();
}

$msg = Message::getMessage();
echo "<div id= 'message'> $msg </div>";



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>City</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <style>
        form{
            width: 600px;
            margin: 0 auto;
        }
        .bg-color{
            background-color: #3c3c3c;
            color: #c0c0c0;
        }
        input{
            color: #000000;
        }

    </style>
</head>
<body>
<div class="container bg-color">
<form action="storeCity.php" method="post">
    <h4>Enter The Name:</h4>
    <input type="text" name="user_name">
    <h4>Enter The Name of City:</h4>
    <input type="text" name="city">
    <h4>Enter Post Code:</h4>
    <input type="number" name="post_code">
    <h4>Enter Post Office:</h4>
    <input type="text" name="post_office">
    <h4>Enter Police Station:</h4>
    <input type="text" name="police_station">
    <h4>Enter Detail Address:</h4>
    <input type="text" name="detail_address">
    <input type="submit" class="btn btn-primary button">
    </form>
</div>

</body>
</html>