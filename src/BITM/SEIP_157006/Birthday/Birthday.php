<?php

namespace App\Birthday;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Birthday extends DB
{
    private $id;
    private $name;
    private $birthday;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("birthday",$allPostData)){
            $this -> birthday = $allPostData['birthday'];
        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->birthday);
        $query = 'INSERT INTO birthday (user_name, birthday) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }
    public function index()
    {

        $sql = "SELECT * from birthday WHERE soft_delete= 'No' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function trash()
    {

        $sql = "SELECT * from birthday WHERE soft_delete ='Yes' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from birthday WHERE user_id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }
    public  function update(){
        $arrayData= array($this-> name,$this->birthday);
        $query = 'UPDATE birthday SET user_name=?, birthday=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }
        Utility::redirect('index.php');
    }
    public function soft_delete(){
        $arrayData= array("Yes");
        $query = 'UPDATE birthday SET soft_delete=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been trashed successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been trashed!");
        }
        Utility::redirect('trashed.php');
    }
    public function recover(){
        $arrayData= array("No");
        $query = 'UPDATE birthday SET soft_delete=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!");
        }
        Utility::redirect('index.php');
    }
    public function delete(){
        $arrayData= array("Yes");
        $query = 'DELETE FROM birthday WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from birthday  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from birthday  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }
}