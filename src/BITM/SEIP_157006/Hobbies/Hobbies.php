<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 1:39 AM
 */

namespace App\Hobbies;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Hobbies extends DB
{
    private $id;
    private $name;
    private $hobbies;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("hobbies1",$allPostData)){
            $this->hobbies = $allPostData['hobbies1'].",";
        }
        if(array_key_exists("hobbies2",$allPostData)){
            $this->hobbies = $this->hobbies.",".$allPostData['hobbies2'];
        }
        if(array_key_exists("hobbies3",$allPostData)){
            $this->hobbies = $this->hobbies.",".$allPostData['hobbies3'];
        }
        if(array_key_exists("hobbies4",$allPostData)){
            $this->hobbies = $this->hobbies.",".$allPostData['hobbies4'];
        }
        if(array_key_exists("hobbies5",$allPostData)){
            $this->hobbies = $this->hobbies.",".$allPostData['hobbies5'];
        }
        if(array_key_exists("hobbies6",$allPostData)){
            $this->hobbies = $this->hobbies.",".$allPostData['hobbies6'];
        }
        //$this -> hobbies = $allPostData['hobbies1'].",".$allPostData['hobbies2'].",".$allPostData['hobbies3'].",".$allPostData['hobbies4'].",".$allPostData['hobbies5'].",".$allPostData['hobbies6'];

    }


    public function store(){
        $arrayData = array($this-> name,$this->hobbies);
        $query = 'INSERT INTO hobbies (user_name, hobbies) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }
    public function index()
    {

        $sql = "SELECT * from hobbies WHERE soft_delete='No' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from  hobbies WHERE user_id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }
    public function trash()
    {

        $sql = "SELECT * from hobbies WHERE soft_delete ='Yes' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public  function update(){
        $arrayData = array($this-> name,$this->hobbies);
        $query = 'UPDATE hobbies SET user_name=?,hobbies=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }
        Utility::redirect('index.php');
    }
    public function soft_delete(){
        $arrayData= array("Yes");
        $query = 'UPDATE hobbies SET soft_delete=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been trashed successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been trashed!");
        }
        Utility::redirect('trashed.php');
    }
    public function recover(){
        $arrayData= array("No");
        $query = 'UPDATE hobbies SET soft_delete=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!");
        }
        Utility::redirect('index.php');
    }
    public function delete(){
        $arrayData= array("Yes");
        $query = 'DELETE FROM hobbies WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from hobbies  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from hobbies  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

}