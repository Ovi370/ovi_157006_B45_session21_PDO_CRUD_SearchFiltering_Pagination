<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 12:58 AM
 */

namespace App\City;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class City extends DB
{
    private $id;
    private $name;
    private $city;
    private $postCode;
    private $postOffice;
    private $policeStation;
    private $detailAddress;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this -> name = $allPostData['user_name'];
        }
        if(array_key_exists("city",$allPostData)){
            $this -> city = $allPostData['city'];
        }
        if(array_key_exists("post_code",$allPostData)){
            $this-> postCode = $allPostData['post_code'];
        }
        if(array_key_exists("post_office",$allPostData)){
            $this-> postOffice = $allPostData['post_office'];
        }
        if(array_key_exists("police_station",$allPostData)){
            $this-> policeStation = $allPostData['police_station'];
        }
        if(array_key_exists("detail_address",$allPostData)){
            $this-> detailAddress = $allPostData['detail_address'];
        }
    }
    public function store(){
        $arrayData = array($this-> name,$this->city, $this-> postCode, $this-> postOffice, $this-> policeStation, $this->detailAddress);
        $query = 'INSERT INTO city (user_name, 	city, post_code, post_office, police_station, detail_address) VALUES (?,?,?,?,?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }
    public function index()
    {

        $sql = "SELECT * from city WHERE soft_delete='No' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from city WHERE user_id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }
    public function trash()
    {

        $sql = "SELECT * from city WHERE soft_delete ='Yes' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public  function update(){
        $arrayData= array($this->city, $this-> postCode, $this-> postOffice, $this-> policeStation, $this->detailAddress);
        $query = 'UPDATE city SET city=?, post_code=?, post_office=?, police_station=?, detail_address=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }
        Utility::redirect('index.php');
    }
    public function soft_delete(){
        $arrayData = array("Yes");
        $query = ' UPDATE city SET soft_delete=? WHERE user_id='.$this->id ;
        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been trashed successfully");
        }
        else{
            Message::setMessage("Failed! Data has not been trashed successfully");
        }
        Utility::redirect('trashed.php');
    }
    public function recover(){
        $arrayData = array("No");
        $query = 'UPDATE city SET soft_delete=? WHERE user_id='.$this->id;
        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);
        if($result){
            Message::setMessage("Success! Data has been Recovered successfully");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered successfully");
        }
        Utility::redirect('index.php');
    }
    public function delete(){
        $arrayData= array("Yes");
        $query = 'DELETE FROM city WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from city  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from city  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }

}