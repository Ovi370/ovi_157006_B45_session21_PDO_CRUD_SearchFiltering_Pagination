<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/25/2017
 * Time: 3:25 PM
 */

namespace App\BookTitle;


use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class BookTitle extends DB
{
    private $book_id;
    private $book_name;
    private $author_name;
    private $soft_deleted;

    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this -> book_id = $allPostData['id'];
        }
        if(array_key_exists("bookName",$allPostData)){
            $this -> book_name = $allPostData['bookName'];
        }
        if(array_key_exists("authorName",$allPostData)){
            $this -> author_name = $allPostData['authorName'];
        }
    }

    public function store(){
        $arrayData= array($this-> book_name,$this->author_name);
        $query = 'INSERT INTO book_title (book_name, author_name) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }


    public function index()
    {

            $sql = "SELECT * from book_title WHERE soft_delete ='No' ";
            $STH = $this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);
            return $STH->fetchAll();


    }

    public function trash()
    {

        $sql = "SELECT * from book_title WHERE soft_delete ='Yes' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from book_title WHERE book_id=".$this->book_id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }
    public function update(){
        $arrayData= array($this-> book_name,$this->author_name);
        $query = 'UPDATE book_title SET book_name=?, author_name=? WHERE book_id='.$this->book_id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }
        Utility::redirect('index.php');
    }

    public function soft_delete(){
        $arrayData= array("Yes");
        $query = 'UPDATE book_title SET soft_delete=? WHERE book_id='.$this->book_id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been trashed successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been trashed!");
        }
        Utility::redirect('trashed.php');
    }
    public function recover(){
        $arrayData= array("No");
        $query = 'UPDATE book_title SET soft_delete=? WHERE book_id='.$this->book_id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!");
        }
        Utility::redirect('index.php');
    }
    public function delete(){
        $arrayData= array("Yes");
        $query = 'DELETE FROM book_title WHERE book_id='.$this->book_id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('index.php');
    }

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }



}