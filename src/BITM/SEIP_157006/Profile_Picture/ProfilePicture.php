<?php
/**
 * Created by PhpStorm.
 * User: Ovi
 * Date: 1/29/2017
 * Time: 2:00 AM
 */

namespace App\Profile_Picture;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;


class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $filename;
    private $tempLocation;
    private $success;
    private $error;


    public function setData($allPostData=null){
        if(array_key_exists("id",$allPostData)){
            $this-> id = $allPostData['id'];
        }
        if(array_key_exists("user_name",$allPostData)){
            $this-> name = $allPostData['user_name'];
        }

    }
    public function setProPic($allFileData=null){
        if(array_key_exists("file",$allFileData)){



            $this-> filename = $allFileData['file']['name'];
            $this-> tempLocation= $allFileData['file']['tmp_name'];
            $src ="images/".time().$this->filename;
            $this-> success = move_uploaded_file($this -> tempLocation,$src);
            $this-> error= $allFileData['file']['error'];
            if($this-> success || $this-> error)
            {
                echo "successfully moved";
                echo "<hr>";
            }
            else
            {
                echo "An error has occured";
                echo "<hr>";
            }

        }
    }
    public function store(){
        $arrayData = array($this-> name,time().$this->filename);
        $query = 'INSERT INTO profile_picture (user_name, profile_picture) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been inserted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }
        Utility::redirect('index.php');
    }
    public function index()
    {

        $sql = "SELECT * from profile_picture WHERE soft_delete='No' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function view()
    {

        $sql = "SELECT * from profile_picture WHERE user_id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();


    }
    public function trash()
    {

        $sql = "SELECT * from profile_picture WHERE soft_delete ='Yes' ";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();


    }
    public function update(){
        $arrayData = array($this-> name,time().$this->filename);
        $query = 'UPDATE profile_picture SET user_name=?, profile_picture=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Updated!");
        }
        Utility::redirect('index.php');
    }

    public function soft_delete(){
        $arrayData= array("Yes");
        $query = 'UPDATE profile_picture SET soft_delete=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been trashed successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been trashed!");
        }
        Utility::redirect('trashed.php');
    }
    public function recover(){
        $arrayData= array("No");
        $query = 'UPDATE profile_picture SET soft_delete=? WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Recovered successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!");
        }
        Utility::redirect('index.php');
    }
    public function delete(){
        $arrayData= array("Yes");
        $query = 'DELETE FROM profile_picture WHERE user_id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH-> execute($arrayData);

        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('index.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from profile_picture  WHERE soft_delete = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }
    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from profile_picture  WHERE soft_delete = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }
}